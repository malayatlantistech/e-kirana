<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class admin_middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->route('admin_login');
        }

        // if (Auth::user()->role == 'USER') {
        //     return redirect()->route('user');
        // }

        if (Auth::user()->role == 'ADMIN') {
            return $next($request);
        }    
    }
}
