<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\user_wallet;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Validator;
use Session;
use DB;
class UserController extends Controller
{
    public $successStatus = 200;

    public function login(Request $request){
        Log::info($request);
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            return view('home');
        }
        else{
            return Redirect::back ();
        }
    }

    public function smsRequest($phone,$msg)
    {
        $client = new \GuzzleHttp\Client();
              $url = "http://weberleads.in/http-tokenkeyapi.php?authentic-key=".env('WEBERLEADS_SMS_API')."&senderid=".env('WEBERLEADS_SMS_SENDERID')."&route=2&number=91".$phone."&message=".$msg;
        $request = $client->get($url);
        $response = $request->getBody();
    }



    public function loginWithOtp(Request $request)
    {

        if(is_numeric(request('mobile')))
        {

            Log::info($request);
            $user  = User::where([['mobile','=',request('mobile')],['otp','=',request('otp')]])->first();
            
        }
        else
        {
            Log::info($request);
            $user  = User::where([['email','=',request('mobile')],['otp','=',request('otp')]])->first();
            

        }
      
        if( $user){
            Auth::login($user, true);
            User::where('mobile','=',$request->mobile)->update(['otp' => null]); 
            return redirect('/');
        }

    }
    public function loginWithOtp1(){
        return view('auth/OtpLogin');
    }
    public function register1(Request $request)
    {
        return view('auth/mediator_register');
    }
    
    public function register(Request $request)
    {
        $input = $request->all();
        $re=User::create($input);
        Log::info($request);
        $user  = User::where([['mobile','=',request('mobile')]])->first();
        Auth::login($user, true);
        $re=new user_wallet;
        $re->wallet_ammount=0;
        $re->user_id=Auth::user()->id;
        $re->save();
        return redirect('/');
    }


    public function sendOtp(Request $request){

        $otp = rand(111111,999999);
       //$otp =123456;
        Log::info("otp = ".$otp);
        if(is_numeric(request('mobile')))
        {
            $user= User::where('mobile','=',$request->mobile)->first();
            if($user)
            {
            User::where('mobile','=',$request->mobile)->update(['otp' => $otp]);
            self::smsRequest($request->mobile,"Please Use This OTP ".$otp." for Complete the Login Process of Darbhangae Shop.");
            $r=1;
            echo $user."|".$otp."|". $r."|";
            }
            else{
                $user=0;
                echo $user."|".$otp."|". $r."|";
            }
        }
        else{
           
            $user= User::where('email','=',$request->mobile)->first();
            if($user)
            {
            User::where('email','=',$request->mobile)->update(['otp' => $otp]);
            self::smsRequest($request->mobile,"Please Use This OTP ".$otp." for Complete the Login Process of Darbhangae Shop.");
            $r=2;
            echo $user."|".$otp."|". $r."|";
            }
            else{
                $user=0;
                echo $user."|".$otp."|". $r."|";
            }

        }
       
      
        
      

       // return response()->json([$user],200);
    }



    /********* Admin Login********** */
       
    public function admin_login(){

        $admin=DB::table('users')->where('role','ADMIN')->first();
        return view('admin.auth.login')->with('admin',$admin);
    }




    public function user_register_otp(Request $req)
    {
        $input = $req->all();
        $validator = Validator::make($input, [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'mobile' => ['required', 'integer', 'digits:10', 'unique:users'],
                'terms' => ['accepted'],
            ]);

        if ($validator->fails()) 
        {
           
            return response()->json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()], 400);
        }
        else
        {
            $otp = rand(100000,999999);
           // $otp=123456;
            self::smsRequest($request->mobile,"Please Use This OTP ".$otp." for Complete the Registration Process of Darbhangae Shop.");
            return response()->json(['success' => true, 'message' =>'Otp Send Your Mobile No.','otp11'=>$otp],200);

        }
    }
}
