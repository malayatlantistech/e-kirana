<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use DB;
use Auth;
use App\cart;
use App\none_cart;
use App\subscriber;
use App\wishlist;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Brian2694\Toastr\Facades\Toastr;
use App\user_wallet;
use App\User;
use App\bank_details;
use App\delivery_boy_pincode;
use App\delivery_wallet;
use App\delivery_wallet_transaction;
use App\withdradal_request;
use Validator; 
use Redirect;
use Image;
use Libern\QRCodeReader\QRCodeReader;

class DeliveryBoyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('delivery_middleware');
    // }

    // public function __construct()
    // {
    //     $this->middleware('verified');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function smsRequest($phone,$msg)
    {
        $client = new \GuzzleHttp\Client();
              $url = "http://weberleads.in/http-tokenkeyapi.php?authentic-key=".env('WEBERLEADS_SMS_API')."&senderid=".env('WEBERLEADS_SMS_SENDERID')."&route=2&number=91".$phone."&message=".$msg;
        $request = $client->get($url);
        $response = $request->getBody();
    }
     public function contact_us()
     {
        return view('delivery_boy.contact');
     }
    public function index()
    {
        //dd(env("DELIVERY_BOY_APP_URL"));

        $delivery_id=Auth::user()->id;

        $pending_booking_count=DB::table('book_multi_items')->where('delivery_boy_id',$delivery_id)->where('order_status',1)->orwhere('order_status',2)->orwhere('order_status',3)->count();
      



        return view('delivery_boy.index',['pending_booking_count'=>$pending_booking_count]);
    }
    //dashboard view
    public function dashboard()
    {
           if(Auth::user()->block=='YES'){
              Auth::logout();
        Session::flash('message', 'You Are Blocked By Admin Please Contact To the Admin.'); 
        return redirect('/delivery_login');
        }else{
           $id=Auth::user()->id;
           $delivery_id=Auth::user()->id;
           $pending_booking_count=DB::table('book_multi_items')->where('delivery_boy_id',$delivery_id)->where('order_status',1)->orwhere('order_status',2)->orwhere('order_status',3)->count();
           $pending_return_count=DB::table('book_multi_items')->where('delivery_boy_id',$delivery_id)->where('order_status',5)->orwhere('order_status',6)->count();
           $complete_booking_count=DB::table('book_multi_items')->where('delivery_boy_id',$delivery_id)->where('order_status',4)->count();
           $complete_return_count=DB::table('book_multi_items')->where('delivery_boy_id',$delivery_id)->where('order_status',6)->count();
      

              $pin_code_count=delivery_boy_pincode::where('delivery_boy_id',$id)->count();
            //  $pin_code_count=delivery_boy_pincode::where('delivery_boy_id',$id)->count();
              return view('delivery_boy.dasbord',['pin_code_count'=>$pin_code_count,'pending_booking_count'=>$pending_booking_count,'pending_return_count'=>$pending_return_count,'complete_booking_count'=>$complete_booking_count,'complete_return_count'=>$complete_return_count]);
        }
      
      
    }
    //login view
    public function delivery_login()
    {
        return view('delivery_boy.auth.login');
    }
     //delivery_login_otp view
    public function delivery_login_mobile()
    {
        return view('delivery_boy.auth.OtpLogin');
    }

    //login Register View
    public function delivery_register()
    {
        return view('delivery_boy.auth.register');
    }
    //otp
    public function delivery_sendOtp(Request $request){
        $otp = rand(100000,999999);
     //   $otp=123456;
        Log::info("otp = ".$otp);
       
        $user1 = User::where('mobile','=',$request->mobile)->where('block','NO')->first();
        if($user1)
        {
            $user = User::where('mobile','=',$request->mobile)->update(['otp' => $otp]);
            if($user){
                self::smsRequest($request->mobile,"Please Use This OTP ".$otp." for Complete the Login Process of Darbhangae Shop.");
                echo $user."|".$otp;
            }
        }
        else{
            echo "2|0";
        }
    }
    //delivery Boy Login

    public function loginWithOtp_driver(Request $request){
        Log::info($request);

       
       $user  = User::where([['mobile','=',request('mobile')],['otp','=',request('otp')]])->first();
     
     

      if( $user){
            Auth::login($user, true);
            User::where('mobile','=',$request->mobile)->update(['otp' => null]);
           
            if($_POST["role"]==Auth::user()->role){

                switch(Auth::user()->role){
                    case 'ADMIN':
                      
                        return redirect('/');
                        break;
        
                    case 'USER':
        
                        
                        return redirect('/');
                        break;
        
                    case 'DELIVERY':

                     
                        if(Auth::user()->block=='YES')
                        {
                            Auth::logout();
                            Session::flash('message', 'You Are Blocked By Admin Please Contact To the Admin.'); 
                            $this->redirectTo = '/delivery_login_mobile';
                            return $this->redirectTo;
                        }
                        elseif(Auth::user()->block=='NO')
                        {

                            return redirect('/');
                            break;
                        }
                    default:
                    return redirect('/');
                        return $this->redirectTo;
                }
                
            }else{
              
                Auth::logout();
               
                switch($_POST["role"]){
                    case 'ADMIN':
                        Session::flash('message', 'This is a Admin Dashboard. Here only admin can login and you not admin!'); 
                        $this->redirectTo = '/admin_login';
                    return $this->redirectTo;
                        break;
                    case 'USER':
                        Session::flash('message', 'This is a User Dashboard. Here only User can login and you not a User!'); 
                  
                        $this->redirectTo = '/login';
                        return $this->redirectTo;
                        break;
                    case 'DELIVERY':
                        Session::flash('message', 'This is a Delivery Boy Dashboard. Here only Delivery Boy can login and you not a Delivery Boy!'); 
                    
                        $this->redirectTo = '/login';
                        return $this->redirectTo;
                        break;
                 
                    default:
                        $this->redirectTo = '/login';
                        return $this->redirectTo;
               }
           }
        


        }
        else{
            return Redirect::back ();
        }
    }

    //logout
    protected function delivery_logout(){

        Auth::logout();
        return redirect()->route('delivery_login');
    }
     //profile
    protected function delivery_profile(){
      
        return view('delivery_boy.profile');
       
    }
    //change password view
    protected function delivery_change_password(){
      
        return view('delivery_boy.change_password');
       
    }
     //edit profile code
    protected function edit_profile(Request $req){
        $name=$req->name;
        $img=$req->profile_image;
        $address=$req->address;
        $id=Auth::user()->id;

       
            $validator = request()->validate([
            'profile_image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'address' => 'required',
            'name' => 'required',
            ]);

        // if($validator->fails()){
        //         return response()->json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()], 400);
        //     }


        if(isset($img))
        {
            $image=$req->file('profile_image');
            $image_name = uniqid() . '.' . $image->getClientOriginalExtension();
            $destinationPath = 'profile_image';
            $resize_image = Image::make($image->getRealPath());
            $resize_image->resize(416,420, function($constraint){
            })->save($destinationPath . '/' . $image_name);  

           $update = User::where('id', $id)->update(['name'=>$name,'profile_image'=>$image_name,'address'=>$address]);
        }
        else
        {
            $update = User::where('id', $id)->update(['name'=>$name,'address'=>$address]);
        }


        if($update){
            return response()->json(['success' => true,'msg'=>'Service Updated Successfull'], 200);
        }
    }



         protected function delivery_change_password_password(Request $request)
        {
        
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
                'password' => 'required|min:8',
                'confirm' => ['same:password'],
            ], [
                'current_password.required' => 'Please enter Your Current Password',
                'password.required' => 'Please enter New Password',
                'confirm.same' => 'Confirm Password do not match',
            ]);

            // check validation
            
            $tr=User::find(auth()->user()->id)->update(['password'=> Hash::make($request->password)]);
            if($tr)
            {
            return response()->json(['success' => true,'msg'=>'Password Change Successfully'], 200);
            }
        }
    

    
    

    public function add_pincode(){

        $pin=DB::table('pincodes')
        ->where('active_status','YES')->get();

        return view('delivery_boy.add_pincode')->with('pin',$pin);
    }


    public function add_pincode_action(Request $req){

        $this->validate($req,[
            'pincode' => 'required',
        ]);
      
        $pin=$req->input('pincode');
       
        for($i=0;$i<count($pin);$i++)
        {
            $pin1=DB::table('delivery_boy_pincodes')->where('pincode',$pin[$i])->where('delivery_boy_id',Auth::user()->id)->count();
            if($pin1==0){
                $pincode= new delivery_boy_pincode;
                    
                $pincode->pincode=$pin[$i];
                $pincode->delivery_boy_id=Auth::user()->id;
                $pincode->remember_token=$req->input('_token');
            $a=$pincode->save();
            }else{
                $a=2;
            }
          


        }

        if($a==1){
            return response()->json(['success' => true, 'message' =>'Pincode add successfully','a'=>1],200);
         }elseif($a==2){
                return response()->json(['success' => false, 'message' =>'Pincode already Added','a'=>2],200);
            
        }else{
            return response()->json(['success' => false, 'message' =>'Somethings Wrong!! Please try again later.','a'=>3],200);
        }

    }
    public function bank_details()
    {
        $delivery_id=Auth::user()->id;
       $rte=bank_details::where('delivery_id',$delivery_id)->first();

     return view('delivery_boy.bankdetails',['details'=>$rte]);
    }

    public function add_bank_details_code(Request $req)
    {
        $delivery_id=Auth::user()->id;
        $a_h_name=$req->a_h_name;
        $bank_name=$req->bank_name;
        $b_name=$req->b_name;
        $a_no=$req->a_no;
        $ifcs_code=$req->ifcs_code;
        $upi_id=$req->upi_id;

        $validator = request()->validate([
            'a_h_name' => 'required',
            'bank_name' => 'required',
            'b_name' => 'required',
            'a_no' => 'required|integer',
            'ifcs_code' => 'required',
            'upi_id' => 'required',
        ],[
            'a_h_name.required'=>'Please enter Account Holder Name',
            'b_name.required'=>'Please enter Brach Name',
            'bank_name.required'=>'Please enter Bank Name',
            'a_no.required'=>'Please enter Account Number',
            'a_no.integer'=>'Account Number must be integer',
            'ifcs_code.required'=>'Please enter IFSC Code',
            'upi_id.required'=>'Please enter your UPI ID'
        ]);
        $tr=bank_details::where('delivery_id',$delivery_id)->count();
        if($tr==0)
        {
        $b_d=new bank_details; 
        $b_d->a_h_name=$a_h_name;
        $b_d->qr_code= $bank_name;
        $b_d->b_name= $b_name;
        $b_d->a_no=$a_no;
        $b_d->ifcs_code=$ifcs_code;
        $b_d->upi_id=$upi_id;
        $b_d->delivery_id=$delivery_id;
        $re=$b_d->save();
        $dd='Bank Details Add Successfully';
        }
        else
        {
           $re=bank_details::where('delivery_id',$delivery_id)->update(['a_h_name'=>$a_h_name,'qr_code'=>$bank_name,'b_name'=>$b_name,'a_no'=>$a_no,'ifcs_code'=>$ifcs_code,'upi_id'=>$upi_id,]);
           $dd='Bank Details Update Successfully';
        }


        if($re==1)
        {
            $gs=bank_details::where('delivery_id',$delivery_id)->first();
            return response()->json(['success' => true,'gs'=>$gs,'msg'=>$dd], 200);
        }
      

    }

    public function delivery_pincode(){
        $pin1=DB::table('delivery_boy_pincodes')->where('delivery_boy_id',Auth::user()->id)->get();
        return view('delivery_boy.delivery_pincode')->with('pin',$pin1);
    }

    public function driver_wallet()
    {
        $driver_id=Auth::user()->id;
        $driver_wallet=delivery_wallet::where('delivery_id',$driver_id)->first();
        $driver_wallet_transaction=delivery_wallet_transaction::where('delivery_id',$driver_id)->orderby('delivery_wallet_transaction_id','desc')->get();
        return view('delivery_boy.wallet',['driver_wallet'=>$driver_wallet,'driver_wallet_transaction'=>$driver_wallet_transaction]);
    }
    public function amount_withdrawal()
    {
        $id= Auth::user()->id;
        $wallet=delivery_wallet::where('delivery_id',$id)->first();
        $wallet_amount= $wallet->wallet_ammount;
        return view('delivery_boy.withdrawl',['wallet_amount'=>$wallet_amount]);   
    }
    public function withdrawal_request_code(Request $req)
    {
        $id= Auth::user()->id;
        $withdrawal_amount=$req->withdrawal_amount;
        $wallet=delivery_wallet::where('delivery_id',$id)->first();
        $wallet_amount= $wallet->wallet_ammount;
        if($wallet_amount>=$withdrawal_amount)
        {
           $tr=new withdradal_request;
           $tr->delivery_id= $id;
           $tr->withdrawal_amount=$withdrawal_amount;
           $tr->save();
           return response()->json(['success' => true,'msg'=>'Withdrawal request Send successfully'], 200);
        }
        else
        {
            return response()->json(['success' => true,'msg'=>'Your Wallet Amount is too Low'], 200);

        }



        
    }

public function delivery_pin_status_change(Request $req){
 //  echo $req->status;
    if($req->status=='true'){
            $status='YES';
    }else{
        $status='NO';
    }
    
    $a=DB::table('delivery_boy_pincodes')->where('id',$req->id)->update(['active_status'=>$status]);
 
    if($a==1){
        return response()->json(['success' => true, 'message' =>'Pincode status successfully changed.','type'=>'black'],200);
    }else{
        return response()->json(['success' => true, 'message' =>'Somethings Wrong!! Please try again later.','type'=>'error'],200);
    }

}
    
public function pending_delivery(Request $req)
{

    $booking_details=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','products.product_id','=','book_multi_items.product_id')
    ->where('book_multi_items.delivery_boy_id',Auth::user()->id)
    ->where('book_multi_items.order_status',1)
    ->orwhere('book_multi_items.order_status',2)
    ->orwhere('book_multi_items.order_status',3)
    ->orderby('book_multi_items.multi_id','desc')
    ->select(['bookings.*','products.product_name','book_multi_items.multi_id','book_multi_items.product_price','book_multi_items.product_id','book_multi_items.gst','users.name','users.email'])
    ->get();

    $booking_details_count=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','products.product_id','=','book_multi_items.product_id')
    ->where('book_multi_items.delivery_boy_id',Auth::user()->id)
    ->where('book_multi_items.order_status',1)
    ->orwhere('book_multi_items.order_status',2)
    ->orwhere('book_multi_items.order_status',3)
    
    ->count();
  
 
 return view('delivery_boy.pending_booking')->with('booking',$booking_details)->with('booking_details_count',$booking_details_count);
}

public function complete_delivery(Request $req)
{
    $booking_details=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','products.product_id','=','book_multi_items.product_id')
    ->where('book_multi_items.delivery_boy_id',Auth::user()->id)
    ->where('book_multi_items.order_status','>=',4)

    ->orderby('book_multi_items.multi_id','desc')
    ->select(['bookings.*','products.product_name','book_multi_items.multi_id','book_multi_items.delivery_date','book_multi_items.product_price','book_multi_items.product_id','book_multi_items.gst','users.name','users.email'])
    ->get();

    $booking_details_count=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','products.product_id','=','book_multi_items.product_id')
    ->where('book_multi_items.delivery_boy_id',Auth::user()->id)
    ->where('book_multi_items.order_status','>=',4)
   
    ->count();
  
 
 return view('delivery_boy.complete_booking')->with('booking',$booking_details)->with('booking_details_count',$booking_details_count);
}
public function pending_return_delivery(Request $req)
{
    $booking_details=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','products.product_id','=','book_multi_items.product_id')
    ->where('book_multi_items.return_delivery_boy_id',Auth::user()->id)
    ->where('book_multi_items.order_status',5)
    ->orwhere('book_multi_items.order_status',6)

    ->orderby('book_multi_items.multi_id','desc')
    ->select(['bookings.*','products.product_name','book_multi_items.multi_id','book_multi_items.product_price','book_multi_items.product_id','book_multi_items.gst','users.name','users.email'])
    ->get();

    $booking_details_count=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','products.product_id','=','book_multi_items.product_id')
    ->where('book_multi_items.return_delivery_boy_id',Auth::user()->id)
    ->where('book_multi_items.order_status',5)
    ->orwhere('book_multi_items.order_status',6)

    ->count();


    
 return view('delivery_boy.pending_return_booking')->with('booking',$booking_details)->with('booking_details_count',$booking_details_count);
}

public function complete_return_delivery(Request $req)
{
    $booking_details=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','products.product_id','=','book_multi_items.product_id')
    ->where('book_multi_items.return_delivery_boy_id',Auth::user()->id)
    ->where('book_multi_items.order_status',7)

    ->orderby('book_multi_items.multi_id','desc')
    ->select(['bookings.*','products.product_name','book_multi_items.multi_id','book_multi_items.delivery_date','book_multi_items.return_date','book_multi_items.product_price','book_multi_items.product_id','book_multi_items.gst','users.name','users.email'])
    ->get();

    $booking_details_count=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','products.product_id','=','book_multi_items.product_id')
    ->where('book_multi_items.return_delivery_boy_id',Auth::user()->id)
    ->where('book_multi_items.order_status',7)
   
    ->count();
 return view('delivery_boy.complete_return_booking')->with('booking',$booking_details)->with('booking_details_count',$booking_details_count);
}


public function booking_details(Request $req)
{
    $booking_details=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','products.product_id','=','book_multi_items.product_id')
    ->where('book_multi_items.delivery_boy_id',Auth::user()->id)
    ->where('book_multi_items.booking_id',$req->booking_id)
    ->where('book_multi_items.multi_id',$req->book_multi_id)
   
    
    ->select(['bookings.*','products.product_name','book_multi_items.multi_id','book_multi_items.product_price','book_multi_items.product_id','book_multi_items.gst','users.name','users.email'])
    ->first();
 return view('delivery_boy.booking_details')->with('booking',$booking_details);
}


public function qr_code_scan(Request $req)
{
 
 return view('delivery_boy.qr_code_scan');
}

public function qr_scan_result(Request $req){
    $QRCodeReader = new QRCodeReader();
     $qrcode_text = $QRCodeReader->decode($req->qr);
    $aa=explode('-',$qrcode_text);
   
   try{


    $booking_details_count=DB::table('book_multi_items')->where('multi_id',$aa[1])->count();
    
if($booking_details_count!=0){
    $booking_details=DB::table('book_multi_items')->where('multi_id',$aa[1])->first();
      if($booking_details->order_status<5){

        $booking_count=DB::table('book_multi_items')->where('multi_id',$aa[1])->where('delivery_boy_id',Auth::user()->id)->count();
        if($booking_count!=0){
            return response()->json(['status'=>1,'booking_id'=>$aa[0],'multi_id'=>$aa[1]], 200);
          
        }else{
            return response()->json(['status'=>0,'booking_id'=>$aa[0],'multi_id'=>$aa[1]], 200);
        }
       

    }else{
        $booking_count=DB::table('book_multi_items')->where('multi_id',$aa[1])->where('return_boy_id',Auth::user()->id)->count();
        if($booking_count!=0){
            return response()->json(['status'=>2,'booking_id'=>$aa[0],'multi_id'=>$aa[1]], 200);
          
        }else{
            return response()->json(['status'=>0,'booking_id'=>$aa[0],'multi_id'=>$aa[1]], 200);
        }
       
    }
}else{
    return 0;
}
} catch (\Exception $e) {
    return response()->json(['status'=>999], 200);
   }
  
}


    public function register_otp(Request $req)
    {
        // $validator = request()->validate([
        //     'name' => ['required', 'string', 'max:255'],
        //     'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        //     'mobile' => ['required', 'integer', 'min:10', 'unique:users'],
        //     'password' => ['required', 'string', 'min:8', 'confirmed'],
        // ]);
        $input = $req->all();
        $validator = Validator::make($input, [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'mobile' => ['required', 'integer', 'digits:10', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'terms' => ['accepted'],
            ]);

      

        
        // if ($validator->fails()) {

        //     return redirect()->back();
        //     return response()->json(['error'=>$validator->errors()], 401);
        // }

        if ($validator->fails()) 
        {
           // return response()->json(['error'=>$validator->errors()], 400);
            return response()->json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()], 400);
        }
        else
        {
             $otp = rand(111111,999999);
            self::smsRequest($request->mobile,"Please Use This OTP ".$otp." for Complete the Login Process of Darbhangae Shop.");
            return response()->json(['success' => true, 'message' =>'Otp Send Your Mobile No.','otp11'=>$otp],200);

        }
    }
    public function experiment()
    {


     return view('delivery_boy.experiment');
    }

    public function experiment_code(Request $req)
    {



    }

    public function driver_order_status(Request $req)
    {
    
      $booking_id=$req->booking_id;
       $booking_multi_id=$req->book_multi_id;
      $staTUS=DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->first();
      $staTUS1=DB::table('bookings')->where('booking_id',$booking_id)->first();
    
      $order_price=$staTUS1->price;
      $wallet_price=$staTUS1->wallet_amount;
      $product_price=$staTUS->product_price+$staTUS->gst;
      $user_id=$staTUS1->customer_id;
      $order_id=$staTUS1->order_id;
      
   
    
    if($staTUS->order_status==2)
      {
      
    
        DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->update(['book_multi_items.order_status'=>$staTUS->order_status+1]);

    
      }
      elseif($staTUS->order_status==3){
    
    
        DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->update(['book_multi_items.order_status'=>$staTUS->order_status+1,'book_multi_items.delivery_date'=>date('Y-m-d h:i:s')]);
    
      }
      
    if($staTUS->order_status==5)
    {
    
  
      DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->update(['book_multi_items.order_status'=>$staTUS->order_status+1]);

  
    }
    elseif($staTUS->order_status==6){
  
  
      DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->update(['book_multi_items.order_status'=>$staTUS->order_status+1,'book_multi_items.return_date'=>date('Y-m-d h:i:s')]);
  
    }
      
    
     
    }



}
