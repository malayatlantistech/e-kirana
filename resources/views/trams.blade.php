@extends('layouts.menu')

@section('title')
Terms and Condition : Darbhangae Shop
@endsection

@section('content')

<div class="breadcrumb section pt-60 pb-60">
    <div class="container">
      <h1 class="uppercase">Terms and Condition</h1>
      <ul>
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li class="active">Terms and Conditions</li>
      </ul>
    </div>
  </div>
  <!-- =====  BREADCRUMB END===== -->
  <div class="page-about section">
  <!-- =====  CONTAINER START  ===== -->
  <div class="container">
    <div class="row ">        
      
      <div class="col-lg-12 col-xl-12 mb-20">
        <!-- about  -->
        <div class="row">
          
          <div class="col-md-12">
            <div class="about-text">
              <div class="about-heading-wrap">
                <h2 class="about-heading mb-20 mt-20 py-2">Terms and <span>Conditions </span></h2>
              </div>
              <p>The platform is owned by Darbhangae Shop, having its registered office at india. <br>
                your use of Darbhangae Shop and services and tools are governed by the following terms and conditions ( "Terms of Use" ) as applicable to the Darbhangae Shop including the applicable policies which are incorporated herein by way of reference. By mere use of  the Darbhangae Shop , you shall be contracting with Darbhangae Shop, the owner of the platform. These terms and conditions including the policies constitute your binding obligations,with Darbhangae Shop. </p>
            </div>
          </div>
        </div>
        <!-- =====  What We Do? ===== -->
       
        <!-- =====  end  ===== -->
        <div class="team-section section mt-40">
        <!--Team Carousel -->
        
      
      </div>
      </div>
    </div>
  </div>
  <hr>
</div>



@endsection