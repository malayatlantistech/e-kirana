<style>
@media only screen and (max-width: 600px) {
    #gotobutton{display:none}
    .quantity{ width: 100% !important;}
}
@media only screen and (min-width: 600px) {
 
    .quantity{ max-width: 50px !important;}
}
.quantity{
    border-right: none;
}
.shopping_button{
   color:white;
   background:#3f8dda;
   border: none
}

.shopping_button:hover{
   color:white;
   background:#dc3545;
   border: none
}
.checkout_button{
   color:white;
   background:#3f8dda;
   border: none
}

.checkout_button:hover{
   color:white;
   background:#dc3545;
   border: none
}
.pluse{
    border-radius:4px;
    border-right: none;
    border-top-right-radius: 0px !important;
    border-bottom-right-radius: 0px !important;
    height: 38px;
}
.minus{
    border-radius:4px;
    border-left: none;
    border-top-left-radius: 0px !important;
    border-bottom-left-radius: 0px !important;
    height: 38px;
    margin-left: -5px;
}

</style>
<script>
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
    });
    </script>
@php($user_id=Auth::user()->id)
@php($for_cart_view2=DB::table('carts')->where('user_id',$user_id)->count())

@if($for_cart_view2==0)
<br>
<center> <h2> No Item In Your Basket </h2>
    <a  href="/shop" class="btn btn-success"  style="border: none;background: #3f8dda;color:white"> Continue Shopping</a>
</center><br>
@else
<div class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr>
          <td class="text-center">Image</td>
          <td class="text-left" style="width: 32%;">Product Name</td>
          <td class="text-right">Unit Price</td>
          <td class="text-left">Quantity</td>
         
          <td class="text-right">Total</td>
        </tr>
      </thead>
      <tbody>
        @php($stock=1)
        @php($grand_total=0)
        @foreach($for_cart_view as $key=>$for_cart_view)
        @php($view_product_id2=$for_cart_view->product_id)
        @php($product_image2=DB::table('product_images')->where('product_id',$view_product_id2)->limit(1)->get())
                             
        <tr>
          <td class="text-center">
              
            @foreach($product_image2 as $product_image2)
            <a href="/product/{{$view_product_id2}}"><img style="height: 100px;" class="img-fluid max-width-100 p-1 border border-color-1" src="../product_image/{{$product_image2->image}}" alt="{{$for_cart_view->product_name}}"></a>
            @endforeach
          </td>
          <td class="text-left"> <a href="/product/{{$view_product_id2}}" class="text-gray-90">{{$for_cart_view->product_name}}@if($for_cart_view->weight>0)(@if($for_cart_view->weight>999){{$for_cart_view->weight/1000}}Kg @else {{$for_cart_view->weight}}gm @endif) @endif</a>
          </td>
          <td class="text-right">
              
            @php($progst=$for_cart_view->selling_price*$for_cart_view->gst/100)
                       
          ₹{{$price=$for_cart_view->selling_price+$progst}}
          <input type="hidden" id="database_price-{{$for_cart_view->id}}" value="{{$price}}">
          </td>
          
          <td class="text-left">
            <div style="" class="input-group btn-block">

              <input type="text" min="1" readonly class="form-control quantity" size="1" value="{{$for_cart_view->quantity}}" id="val-{{$for_cart_view->id}}" name="quantity" style="text-align:center ">
              <span class="input-group-btn">
              <button type="button" class="btn pluse" data-toggle="tooltip" data-original-title="Increase" style="padding: 0.17rem 0.75rem;border-color: #c1bcbc;" id="up-{{$for_cart_view->id}}" onclick="up({{$for_cart_view->id}})">+</button>
              <button type="button" class="btn  minus" data-toggle="tooltip" data-original-title="Decrease" style="padding: 0.17rem 0.75rem;border-color: #c1bcbc;" id="down-{{$for_cart_view->id}}" onclick="down({{$for_cart_view->id}})">-</button>
           
            <button  class="btn" title="Update" disabled data-toggle="tooltip" type="submit" data-original-title="Update" id="update_btn-{{$for_cart_view->id}}" onclick="update({{$for_cart_view->id}});"><i class="fa fa-refresh"></i></button>
            <button onclick="remove_item({{$for_cart_view->product_id}});" class="btn btn-danger" title="Remove" data-toggle="tooltip" type="button" data-original-title="Remove"><i class="fa fa-times-circle"></i></button>
            </span></div>
            <input type="hidden" value="{{$for_cart_view->quantity}}" id="original_qty-{{$for_cart_view->id}}">
         
            @if($for_cart_view->available_stock==0)
            @php($stock=0)
            <span style="font-size: 12px;color: red;font-weight: bolder;">Out of Stock</span>
            @elseif($for_cart_view->available_stock<$for_cart_view->quantity)
            @php($stock=0)
            <span style="font-size: 12px;color: red;font-weight: bolder;">Sorry! Only {{$for_cart_view->available_stock}} pics available in our store.</span>
      @endif
          </td>
        
          <td class="text-right">
              
            <span class="" >&#8377;{{$grand_total=$grand_total+$price*$for_cart_view->quantity}}
                {{-- <input type="text" class="tp-{{$key}}" readonly id="total_product_price-{{$for_cart_view->id}}" value="{{$price*$for_cart_view->quantity}}" style="width:max-contant;border:0"> --}}
</span>
          </td>
        </tr>
        @endforeach
        <input type="hidden" id="total_product" value="{{$key}}">
      </tbody>
    </table>
  </div>
 


<div class="row">
  <div class="col-md-6 offset-md-6">
    <table class="table table-bordered">
      <tbody>
   
        <tr>
          <td class="text-right"><strong>Basket Total:</strong></td>
          <td class="text-right"><span class="amount" style="float:right;font-size: 20px;color:black;font-weight:800">&#8377;{{$grand_total}}</span></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<div class="pt-md-3">
    <div class="row">
        <div class="col-md-6" id="gotobutton">
            <!-- Apply coupon Form -->
           <a href="/shop"> <button type="button" class="shopping_button btn ">Goto Shopping</button></a>
 <!-- End Apply coupon Form -->
        </div>
        <div class="col-md-6">
       
           <a href="/checkout" style="float: right;"> <button  class="checkout_button btn" @if($stock==0) disabled @endif style="cursor: pointer">Proceed to checkout</button></a>
           <br><br><center style="color:red;float: right;"><b>@if($stock==0) *Please remove out of stock item from Basket for checkout @endif</b></center>
        </div>
        
    </div>
</div>
  @endif




