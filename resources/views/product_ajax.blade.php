{{$pro_count}}malayssj
@if($pro_count==0)
   <span style="font-weight: 900;color: black; padding-left: 34%;font-size: 30px;padding-top:50px">No Product Found</span>
@else
<div class="row">
                            @foreach($pro as $related_product_details1)
                            @php($product_id=$related_product_details1->product_id)
                            @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                            @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
                            @php($brand=DB::table('brands')->where('brand_id',$related_product_details1->brand_id)->first())
                            @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$related_product_details1->sub_cat_id)->first())

                                <div class="product-layout  product-grid col-lg-3 col-6 ">
                                    <div class="item">
                                    <div class="product-thumb transition">
                                        <div class="image">
                                            <div class="first_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image1->image}}" alt="pure-spice-3" title="{{$related_product_details1->product_name}}" class="img-responsive"> </a> </div>
                                            <div class="swap_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image2->image}}" alt="pure-spice-3" title="{{$related_product_details1->product_name}}" class="img-responsive"> </a></div>
                                        </div>
                                        <div class="product-details">
                                        <div class="caption">
                                            <h4><a href="/shop?brand={{$brand->brand_id}}">{{$brand->brand_name}}</a></h4>
                                            @php($progst=$related_product_details1->selling_price*$sub_cat->gst/100)
                       
                                  <p class="price">₹{{round($related_product_details1->selling_price+$progst)}}</p>
                                            <div class="product_option">
                                            <div class="form-group required ">
                                                <a href="/product/{{$product_id}}"><?php echo substr($related_product_details1->product_name,0,18); ?>@if(strlen($related_product_details1->product_name)>18){{'...'}} @endif</a>
                                  
                                            </div>
                                            <div class="input-group button-group">
                                                <span style="font-size: 10px;">
                                                @php($a=(int)$related_product_details1->review)
                                                @for($i=0;$i<$a;$i++)
                                                <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                                @endfor
                                                @for($i=0;$i<5-$a;$i++)
                                                <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                                @endfor
                                                
                        
                                                ({{$related_product_details1->review}})
                                                </span>
                                                
                                                <button type="button" class="addtocart pull-right addtocartbtn" onclick="cart({{$product_id}});">Add</button>
                                              </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>

                               
                              @endforeach

                            </div>
                          
                                <center>   {!! $pro->links() !!} </center>
                              




                     

                            @endif