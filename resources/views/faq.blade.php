@extends('layouts.menu')

@section('title')
Frequently Asked Questions : Darbhangae Shop
@endsection

@section('content')

<div class="breadcrumb section pt-60 pb-60">
    <div class="container">
      <h1 class="uppercase">Frequently Asked Questions</h1>
      <ul>
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li class="active">Frequently Asked Questions</li>
      </ul>
    </div>
  </div>
  <!-- =====  BREADCRUMB END===== -->
  <div class="page-about section">
  <!-- =====  CONTAINER START  ===== -->
  <div class="container">
    <div class="row ">        
      
      <div class="col-lg-12 col-xl-12 mb-20">
        <!-- about  -->
        <div class="row">
          
          <div class="col-md-12">
            <div class="about-text">
              <div class="about-heading-wrap">
                <h2 class="about-heading mb-20 mt-20 py-2">Frequently Asked <span>Questions </span></h2>
              </div>
              <p>Kindly check the FAQ below if you are not very familiar with the functioning of this website. If your query is of urgent nature and is different from the set
                of questions then do write to us at or call us on between 7 am &amp; 10 pm on all days including Sunday to get our immediate help. </p>
            </div>
          </div>
        </div>
        <!-- =====  What We Do? ===== -->
        <div class="row">
          <div class="col-md-12">
            <div class="heading-part mb-20 mt-40">
              <h3 class="section_title" style=" text-align: left">Registration Process</h3>
            </div>
              <div id="accordion">
              <div class="card my-1">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">1.How do I register?</a>

                  </h5>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body"><p> Customer can easily register by clicking on login button and new customer by providing, email-id and phone number.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingTwo">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">2.Are there any charges for registration?</a>
                  </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                  <div class="card-body"><p>No, for registration no need to pay.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">3.Do I have to necessarily register to shop on darbhangaeshop.com?</a>
                  </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                  <div class="card-body"><p>Yes, it is mandatory to register for darbhangaeshop.com to buy our products.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingThree1">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree1">4.Can I have multiple registrations?</a>
                  </h5>
                </div>
                <div id="collapseThree1" class="collapse" aria-labelledby="headingThree1" data-parent="#accordion">
                  <div class="card-body"><p> No, why because you have registered through phone number.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingThree2">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree2">5.Can I add more than one delivery address in an account?</a>
                  </h5>
                </div>
                <div id="collapseThree2" class="collapse" aria-labelledby="headingThree2" data-parent="#accordion">
                  <div class="card-body"><p>Yes, you can have more than one delivery address.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingThree3">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree3">6.Can I have multiple accounts with same mobile number and email id?</a>
                  </h5>
                </div>
                <div id="collapseThree3" class="collapse" aria-labelledby="headingThree3" data-parent="#accordion">
                  <div class="card-body"><p> No, you cannot because phone number verification process is there so only one account for one mobile number.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingThree4">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree4">7.Can I have multiple accounts for members in my family with different mobile number and email address but same or common delivery address?</a>
                  </h5>
                </div>
                <div id="collapseThree4" class="collapse" aria-labelledby="headingThree4" data-parent="#accordion">
                  <div class="card-body"><p>Yes, you can have many accounts with your family member’s phone number.</p>
                  </div>
                </div>
              </div>
            
            </div>

          </div>
         
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="heading-part mb-20 mt-40">
              <h3 class="section_title" style=" text-align: left">Account Releted</h3>
            </div>
              <div id="accordion1">
              <div class="card my-1">
                <div class="card-header" id="headingOne1">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1">1.What is My Account?</a>

                  </h5>
                </div>

                <div id="collapseOne1" class="collapse" aria-labelledby="headingOne1" data-parent="#accordion1">
                  <div class="card-body"><p>My Account is the section you reach after you log in at Darbhangae Shop.co.in My Account allows you to track your active orders, credit note details as well as see your order history and update your contact details.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingTwo1">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo1">2.How do I reset my password?</a>
                  </h5>
                </div>
                <div id="collapseTwo1" class="collapse" aria-labelledby="headingTwo1" data-parent="#accordion1">
                  <div class="card-body"><p>You need to enter your email address on the Login page and click on forgot password. An email with a reset password will be sent to your email address. With this, you can change your password. In case of any further issues please contact our customer support team.</p>
                  </div>
                </div>
              </div>
            
            
            </div>

          </div>
         
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="heading-part mb-20 mt-40">
              <h3 class="section_title" style=" text-align: left">Payment Releted</h3>
            </div>
              <div id="accordion11">
         
                <div class="card my-1">
                  <div class="card-header" id="headingThree1">
                    <h5 class="mb-0">
                      <a data-toggle="collapse" data-parent="#accordion11" href="#collapseThree1">1.What are the modes of payment?</a>
                    </h5>
                  </div>
                  <div id="collapseThree1" class="collapse" aria-labelledby="headingThree1" data-parent="#accordion11">
                    <div class="card-body"><p>We accept all kind of payment(COD,CREDIT & DEBIT CARD,UPI)
                     </p>
                    </div>
                  </div>
                </div>
                
                
                <div class="card my-1">
                  <div class="card-header" id="headingThree11">
                    <h5 class="mb-0">
                      <a data-toggle="collapse" data-parent="#accordion11" href="#collapseThree11">2.Are there any other charges or taxes in addition to the price shown? Is VAT added to the invoice?</a>
                    </h5>
                  </div>
                  <div id="collapseThree11" class="collapse" aria-labelledby="headingThree11" data-parent="#accordion11">
                    <div class="card-body"><p>  No</p>
                    </div>
                  </div>
                </div>
                <div class="card my-1">
                  <div class="card-header" id="headingThree22">
                    <h5 class="mb-0">
                      <a data-toggle="collapse" data-parent="#accordion11" href="#collapseThree22">3.What is the meaning of cash on delivery?</a>
                    </h5>
                  </div>
                  <div id="collapseThree22" class="collapse" aria-labelledby="headingThree22" data-parent="#accordion11">
                    <div class="card-body"><p>You have to pay the cash after the delivery is done.</p>
                    </div>
                  </div>
                </div>
                <div class="card my-1">
                  <div class="card-header" id="headingThree33">
                    <h5 class="mb-0">
                      <a data-toggle="collapse" data-parent="#accordion11" href="#collapseThree33">4.Where do I enter the coupon code?</a>
                    </h5>
                  </div>
                  <div id="collapseThree33" class="collapse" aria-labelledby="headingThree33" data-parent="#accordion11">
                    <div class="card-body"><p> At checkout page you can add coupon code </p>
                    </div>
                  </div>
                </div>
            
            </div>

          </div>
         
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="heading-part mb-20 mt-40">
              <h3 class="section_title" style=" text-align: left">Delivery Releted</h3>
            </div>
              <div id="accordion2">
              <div class="card my-1">
                <div class="card-header" id="headingOne5">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5">1.When will I receive my order?</a>

                  </h5>
                </div>

                <div id="collapseOne5" class="collapse " aria-labelledby="headingOne5" data-parent="#accordion2">
                  <div class="card-body"><p> You will receive your order within the selected timeslots.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingTwo5">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo5">2.How are the fruits and vegetables packaged?</a>
                  </h5>
                </div>
                <div id="collapseTwo5" class="collapse" aria-labelledby="headingTwo5" data-parent="#accordion2">
                  <div class="card-body"><p>Fresh fruits and vegetables are handpicked and hand cleaned. We ensure hygienic and careful handling of all our products.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingThree5">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseThree5">3.How are the fruits and vegetables weighed?</a>
                  </h5>
                </div>
                <div id="collapseThree5" class="collapse" aria-labelledby="headingThree5" data-parent="#accordion2">
                  <div class="card-body"><p>Every fruit and vegetable varies a little in size and weight. While you shop we show an estimated weight and price for everything priced by kilogram. At the time of delivery we weigh each item to determine final price. This could vary by 5% at maximum. Therefore if you have shopped for something that costs Rs. 100 per kg, and we delivery 1.5 kg of the product to you (eg cabbage, pineapple), you will still be charged a maximum of Rs. 105. In case the weight of the product is lesser than what you ordered, you will pay correspondingly less.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingThree15">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseThree15">4.How will the delivery be done?</a>
                  </h5>
                </div>
                <div id="collapseThree15" class="collapse" aria-labelledby="headingThree15" data-parent="#accordion2">
                  <div class="card-body"><p>We have a dedicated team of delivery personnel and a fleet of vehicles operating across the city which ensures timely and accurate delivery to our customers.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingThree25">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseThree25">5.How do I change the delivery info (address to which I want products delivered)?</a>
                  </h5>
                </div>
                <div id="collapseThree25" class="collapse" aria-labelledby="headingThree25" data-parent="#accordion2">
                  <div class="card-body"><p>At the time of placing order you can change delivery address to which I want products delivered.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingThree35">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseThree35">6.Will someone inform me if my order delivery gets delayed?</a>
                  </h5>
                </div>
                <div id="collapseThree35" class="collapse" aria-labelledby="headingThree35" data-parent="#accordion2">
                  <div class="card-body"><p>Yes, our customer support executive will inform if there is any delay in delivery of your product.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingThree45">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseThree45">7.What is the minimum order for delivery?</a>
                  </h5>
                </div>
                <div id="collapseThree45" class="collapse" aria-labelledby="headingThree45" data-parent="#accordion2">
                  <div class="card-body"><p>The minimum order for delivery is 100rs.</p>
                  </div>
                </div>
              </div>
              
              <div class="card my-1">
                <div class="card-header" id="headingThree43">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion"1 href="#collapseThree43">8.Do you do same day delivery?</a>
                  </h5>
                </div>
                <div id="collapseThree43" class="collapse" aria-labelledby="headingThree43" data-parent="#accordion1">
                  <div class="card-body"><p>Yes, after you placed a delivery within an hour we will proceed with the delivery.</p>
                  </div>
                </div>
              </div>
              <div class="card my-1">
                <div class="card-header" id="headingThree435">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion"1 href="#collapseThree435">9.Is Same Day Delivery applicable to only a few products or all products?</a>
                  </h5>
                </div>
                <div id="collapseThree435" class="collapse" aria-labelledby="headingThree435" data-parent="#accordion1">
                  <div class="card-body"><p>Same day delivery will applicable to all the products.</p>
                  </div>
                </div>
              </div>      
            </div>

          </div>
         
        </div>

        <!-- =====  end  ===== -->
        <div class="team-section section mt-40">
        <!--Team Carousel -->
       
      
      </div>
      </div>
    </div>
  </div>
  <hr>
</div>

@endsection
