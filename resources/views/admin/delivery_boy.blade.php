@extends('admin.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Delivery Boy</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
                                        @if ($message = Session::get('success'))
										<h2 class="mb-0" style="color:red"><b>{{ $message }}</b></h2>	
															@else
															<h2 class="mb-0">Delivery Boy</h2>
													 @endif
											
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
                                                            <th class="wd-15p">Registration <br>Date</th>
															<th class="wd-15p">Customer <br>Name</th>
															<th class="wd-15p">Customer <br>Email</th>
															<th class="wd-15p">Contact No</th>
															<th class="wd-15p">Block<br> Unblock</th>
                                                            <th class="wd-15p">Approve <br>Status</th>
															<th class="wd-15p">View <br>Details</th>
														</tr>
													</thead>
													<tbody>
													@foreach($user as $user)
													<tr>
                                                    <td>{{$user->created_at}}</td>
                                                    <td>
													{{$user->name}}
													<span style="float:right">
													@if($user->status=='NO')


														<a style="color:white" class="btn btn-icon btn-sm btn-info mt-1 mb-1" type="button" data-original-title="Not Approve" data-placement="top"  data-toggle="tooltip">
															<span class="btn-inner--icon"><i class="fe fe-info"></i></span>
                                                        </a>
													@endif
													@if($user->block=='YES')


														<a style="color:white" class="btn btn-icon btn-sm btn-danger mt-1 mb-1" type="button" data-original-title="Block" data-placement="top"  data-toggle="tooltip">
															<span class="btn-inner--icon"><i class="fe fe-info"></i></span>
                                                        </a>
													@endif
														
													</span>
                                                    </td>
                                                    <td>
													{{$user->email}}
                                                    </td>
                                                    <td>
													{{$user->mobile}}
                                                    </td>
													<td>
													<label class="custom-switch">
															<input onchange="user_block({{$user->id}},this.checked);" type="checkbox" name="" value="1" class="custom-switch-input" @if($user->block=='YES') {{'checked=""'}} @endif>
															<span class="custom-switch-indicator custom-switch-indicator-square custom-switch-indicator-lg"></span>
														</label>
													</td>
													<td>
													@if($user->status=='NO')
													<a href="mediator_approve?value=true&id={{$user->id}}"><input type="submit" name="submit" value="Approve" class="btn btn-primary mt-1 mb-1 btn-sm"></a>
													@else
														<span class="badge badge-info ">Approved</span>
													@endif
													</td>
													<td>







														<a type="button" class="btn btn-info mt-1 mb-1 btn-sm" data-toggle="modal" data-target="#modal-default{{$user->id}}" style="color:white">Details</a>
														<div class="modal fade" id="modal-default{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
															<div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
																<div class="modal-content">
																	<div class="modal-header">
																		<h2 class="modal-title" id="modal-title-default">{{$user->name}} Details</h2>
																		<a type="button" class="close" data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">×</span>
																		</a>
																	</div>
																@php($bank=DB::table('users')->join('bank_details','bank_details.delivery_id','=','users.id')->where('users.id',$user->id )->first())
																@php($bank_count=DB::table('users')->join('bank_details','bank_details.delivery_id','=','users.id')->where('users.id',$user->id )->count())
																	<div class="modal-body">
																		Name : {{$user->name}}<br>
																		Phone No. : +91 {{$user->mobile}}<br>
																		Email ID : {{$user->email}}<br>
																		Address : {{$user->address}}<br>
																		<br><b style="font-size:15px">Bank Details</b> <br>
																		@if($bank_count>0)
																	
																		Account Holder Name : {{$bank->a_h_name}}<br>
																		Account Number : {{$bank->a_no}}<br>
																		Bank Name : {{$bank->qr_code}}<br>
																		Branch Name : {{$bank->b_name}}<br>
																		IFSC Code : {{$bank->ifcs_code}}<br>
																		UPI ID : {{$bank->upi_id}}
																		@else
																		<hbr><h2>Not added</h2>
																		@endif

																	</div>
																	
																</div>
															</div>
														</div>

														<a href="delivery_details?delivery_boy_id={{$user->id}}" class="btn btn-icon btn-sm  btn-primary mt-1 mb-1" style="color:white">
														
															<span class="btn-inner--text">Delivery Report</span>
														</a>
														<a href="return_details?delivery_boy_id={{$user->id}}" class="btn btn-icon btn-sm  btn-warning mt-1 mb-1" style="color:white">
														
															<span class="btn-inner--text">Return Report</span>
														</a>

														<a href="/delivered_pincodes?id={{$user->id}}" class="btn btn-secondary mt-1 mb-1 btn-sm">Pincodes</a>
													</td>
													@endforeach
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
                            <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
                            <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
<script>
function user_block(id,value){
	var token = $("#_token").val();
$.ajax({

url:'user_block',


type:'POST',

data:{_token:token,id:id,value:value},

success:function(response)
{

  }   
});


}

</script>
<script>								    
$(document).ready(function() {
   $('#example').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
    
});
</script> 
@endsection