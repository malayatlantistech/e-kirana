@extends('delivery_boy.layouts.menu')
@section('title','Bank Details | Darbhangae Shop')
@section('content')

<section class="content" style="
padding-top: 14px;
" >
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Add Bank Details</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="myForm" action="{{route('add_bank_details_code')}}" enctype="multipart/form-data">
              
              <div class="card-body">
               
                <div class="form-group">
                  <label for="exampleInputEmail1">Account Holder Name</label>
                <input type="text" class="form-control" name="a_h_name" id="a_h_name" value="@if(isset($details->a_h_name)) {{$details->a_h_name}}@endif" placeholder="Account Holder Name">
                </div>
                <div class="text-danger"><strong class="error" id="a_h_name_error"></strong></div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Bank Name</label>
                  <input type="text" class="form-control" id="bank_name" name="bank_name" value="@if(isset($details->qr_code)) {{$details->qr_code}} @endif" placeholder="Bank Name">
                </div>
                <div class="text-danger"><strong class="error" id="bank_name_error"></strong></div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Branch Name</label>
                  <input type="text" class="form-control" id="b_name" name="b_name" value="@if(isset($details->b_name)) {{$details->b_name}} @endif" placeholder="Branch Name">
                </div>
                <div class="text-danger"><strong class="error" id="b_name_error"></strong></div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Account No</label>
                    <input type="text" class="form-control" name="a_no" id="a_no" value="@if(isset($details->a_no)) {{$details->a_no}} @endif" placeholder="Account No">
                </div>
                <div class="text-danger"><strong class="error" id="a_no_error"></strong></div>
                <div class="form-group">
                    <label for="exampleInputPassword1">IFSC code</label>
                    <input type="text" class="form-control" id="ifcs_code" name="ifcs_code" value="@if(isset($details->ifcs_code)) {{$details->ifcs_code}} @endif" placeholder="IFCS code">
                </div>
                <div class="text-danger"><strong class="error" id="ifcs_code_error"></strong></div>
                <div class="form-group">
                    <label for="exampleInputPassword1">UPI Id</label>
                    <input type="text" class="form-control" id="upi_id" name="upi_id" value="@if(isset($details->upi_id)) {{$details->upi_id}} @endif" placeholder="UPI Id">
                </div>
                <div class="text-danger"><strong class="error" id="upi_id_error"></strong></div>
           
                {{-- <div class="form-group">
                    <label for="exampleInputFile">Choose QR code</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="qr_code" name="qr_code">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    
                    </div>
                  </div> --}}
                  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
               
              <!-- /.card-body -->

              <div class="">
                <button type="submit" class="btn btn-primary form-control"  id="update_bttn" >Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card -->

         


       
         

        </div>
        <!--/.col (left) -->
        <!-- right column -->
     
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>






  @endsection
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
     <script>
// $(document).ready(function(){
//     document.getElementById("myForm").onsubmit =(e)=>{
//       e.preventDefault();
//         const url = "{{route('add_bank_details_code')}}";
//         var data = new URLSearchParams();
//          console.log(e.target)
//          for(const pair of new FormData(e.target)){
//            data.append(pair[0],pair[1],pair[2],pair[3],pair[4],pair[5])
//          }

//         fetch(url,{
//             method:"POST",
//             body:data,
           
//         }).then(res=>res.json())
//         .then(alert())
//         .catch((error) => {
//           console.error('Error:', error);
//         }); 
  
// }

//   });















$(document).ready(function(){
   $('#myForm').submit(function(event){
        event.preventDefault();
        $("#update_bttn").prop('disabled', true);
        var formdata = new FormData($(this)[0]);
            $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache:false,
            data: formdata,
            success: function (response) {  
                $('.error').html('');
                if(response.success == true){

                   $('#a_h_name').val(response.gs.a_h_name);
                   $('#bank_name').val(response.gs.qr_code);
                   $('#b_name').val(response.gs.b_name);
                   $('#a_no').val(response.gs.a_no);
                   $('#ifcs_code').val(response.gs.ifcs_code);
                   $('#upi_id').val(response.gs.upi_id);
                    mdtoast(response.msg, { 
                        type: 'ewrty',
                        duration: 3000
                        });
                        $('.error').html('');
                }

            },error: function (jqXHR) {
                $("#update_bttn").prop('disabled', false);
              
                var errormsg = jQuery.parseJSON(jqXHR.responseText);
                $.each(errormsg.errors,function(key,value) {

                    $('#'+key+'_error').html(value);
                });
            }
        });
    });
});

    </script>