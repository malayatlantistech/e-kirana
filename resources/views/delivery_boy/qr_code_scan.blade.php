@extends('delivery_boy.layouts.menu')
@section('title','wallet | Darbhangae Shop')
@section('content')
<style>
input.img {
  width: 0.1px;
	height: 0.1px;
	opacity: 0;
	overflow: hidden;
	position: absolute;
	z-index: -1;
  &:focus + label {
    box-shadow: 0 0 0 2px red;
  }
}
label {
  border-radius: 50%;
  background: green;
  padding: 30px;
  font-size: 5rem;
}
</style>
<div id="reader"></div>

<!-- 
  Or add captured if you only want to enable smartphone camera, PC browsers will ignore it.
-->

  <div style="padding-top:30%">
    <center>
<form id="update_details" method="post" action="qr_scan_result" enctype="multipart/form-data">
  @csrf

<input type="file" accept="image/*" capture="camera" class="img" id="qr-input-file" name="qr"/>
<label for="qr-input-file">📷</label>
</form>
</center>
</div>


@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
   

  $(document).ready(function(){


      $('#update_details').submit(function(event){
            event.preventDefault();
          // $("#update_bttn").prop('disabled', true);
            var formdata = new FormData($(this)[0]);
          
                $.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    cache:false,
                    data: formdata,
                    success: function (response) {  
                     
                      if(response.status==1){
                        window.location.href='/booking_details?booking_id='+response.booking_id+'&book_multi_id='+response.multi_id;
                      }else if(response.status==2){
                        window.location.href='/booking_details?booking_id='+response.booking_id+'&book_multi_id='+response.multi_id+'&return=20';
                      }else if(response.status==999){
                        mdtoast('Please choose a QR Code.', { 
                            type: 'warning',
                            duration: 3000
                        });
                      }else{
                        mdtoast('Somethigs Wrong! Please try again later.', { 
                            type: 'error',
                            duration: 3000
                        });
                      }
                   
                    }
                });

      });
  
  });
  
      </script>

      <script>
  $(document).ready(function(){
    $('#qr-input-file').click();

$('#qr-input-file').change(function(event){
  $('#update_details').submit();
});

});

</script>